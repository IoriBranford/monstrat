﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

public class FreeFlightCameraController : MonoBehaviour {
	private Camera m_Camera;
	[SerializeField] private MouseLook m_MouseLook;

	// Use this for initialization
	void Start () {
		m_Camera = Camera.main;
		m_MouseLook.Init(transform , m_Camera.transform);
	}
	
	// Update is called once per frame
	void Update () {
		m_MouseLook.LookRotation (transform, m_Camera.transform);	
	}
}
